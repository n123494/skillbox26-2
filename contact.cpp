#include "contact.hpp"

Contact::Contact(const std::string& name)
{
    setName(name);
}

Contact::Contact(const std::string& name, const std::string& num) : Contact(name)
{
    addNum(num);
}

Contact::~Contact()
{
}

const std::string Contact::getName() const
{
    return name;
}

const std::vector<std::string>& Contact::getNums() const
{
    return nums;
}

void Contact::setName(const std::string& name)
{
    this->name = name;
}

void Contact::addNum(const std::string& num)
{
    nums.push_back(num);
}

void Contact::removeNum(const unsigned index)
{
    nums.erase(nums.begin() + (index - 1));
}

void Contact::showNums() const
{
    for (auto i : nums)
    {
        std::cout << i << std::endl;
    }
}

size_t Contact::numsLength() const
{
    return nums.size();
}

std::string Contact::getNumber(unsigned index) const
{
    return nums[index - 1];
}

