#pragma once
#include <string>
#include <vector>
#include <iostream>



class Contact
{
    std::string name;
    std::vector<std::string> nums;
    
public:
    Contact(const std::string& name);
    Contact(const std::string& name, const std::string& num);
    ~Contact();

    const std::string getName() const;
    
    const std::vector<std::string>& getNums() const;
    
    void setName(const std::string& name);
    
    void addNum(const std::string& num);
    
    void removeNum(const unsigned index);
    
    void showNums() const;
    
    size_t numsLength() const;
    
    std::string getNumber(unsigned index) const;
};

