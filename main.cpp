#include "phone.hpp"
#include <iostream>
#include <string>
#include <limits>

std::string split(std::string& input)
{
    std::string info = "";
    size_t space = input.find(' ');
    if (space != std::string::npos)
    {
        info = input.substr(space + 1);
        input = input.substr(0, space);
    }    
    return info;
}

int main(int argc, char **argv)
{
    Phone phone;
    
    std::cout << "add - add contact or tel. number\n";
    std::cout << "call - call contact by name or tel. number\n";
    std::cout << "sms - send sms by name or tel. number\n";
    std::cout << "exit - turn off phone\n";
    
    while (true)
    {
        std::cin.sync();
        std::string input;
        std::cout << "Enter command: ";
        std::getline(std::cin, input);
        std::string info = split(input);
        
        if (input == "add"){
            if (info == ""){
                std::cout << "Enter name to create new contact\n";
                std::cout << "Enter tel. number to add num to contact\n";
                std::cout << "Enter both name and tel.number to create new contact or add number to exist contact\n";
                std::cout << "Info: ";
                std::getline(std::cin, info);
            }
            phone.add(info);
        }
        else if (input == "call")
        {
            if (info == "")
            {
                std::cout << "Enter name or tel. number: ";
                std::getline(std::cin, info);
            }
            phone.call(info);
        }
        else if (input == "sms")
        {
            if (info == "")
            {
                std::cout << "Enter name or tel. number: ";
                std::getline(std::cin, info);
            }
            phone.sms(info);
        }
        else if (input == "exit")
        {
            break;
        }
        else
        {
            std::cout << "Invalid input\n";
        }
    }

	return 0;
}
