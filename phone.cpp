#include "phone.hpp"

Phone::Phone()
{
}

Phone::~Phone()
{
}

void Phone::add(const std::string& input)
{
    std::string name = "";
    std::string num = "";
    
    size_t space = input.find(' ');
    if (space != std::string::npos)
    {
        name = input.substr(0, space);
        num = input.substr(space + 1);
        if (!numCheck(num))
        {
            std::cout << "Invalid tel. number\n";
            return;
        }
        else
        {
            create(name, num);
        }
    }
    else
    {
        if (numCheck(input))
        {
            addNum(input);
        }
        else
        {
            std::cout << "Create empty contact '" << input << "' ? y/n: ";
            std::string c;
            std::cin >> c;
            if (c == "y")
            {
                createEmpty(input);
            }
            else
            {
                std::cout << "Operation failed\n";
                return;
            }
        }
    }
}

void Phone::call(const std::string& input) const
{
    std::string num = getNum(input);
    if (num != ""){
        std::cout << "CALL: " << num << std::endl;
    }
}

void Phone::sms(const std::string& input) const
{
    std::string num = getNum(input);
    std::string text;
    if (num != ""){
        std::cout << "Enter your sms: ";
        std::cin >> text;
        std::cout << "SMS was sended to " << num << std::endl;
    }
}

std::string Phone::getNum(const std::string& input) const
{
    if (!numCheck(input))
    {
        auto it = phonebook.find(input);
        if (it != phonebook.end())
        {
            size_t count = it->second.numsLength();
            if (count == 0)
            {
                std::cout << "Contact have no tel. numbers\n";
            }
            if (count == 1)
            {
                return it->second.getNumber(1);
            }
            if (count > 1)
            {
                std::cout << "Choose tel. number (1 - " << count << "):" << std::endl;
                it->second.showNums();
                size_t c;
                std::cout << "Index: ";
                std::cin >> c;
                if (c < 1 || c > count)
                {
                    std::cout << "Invalid index\n";
                }
                else
                {
                    return it->second.getNumber(c);
                }
            }
        }
        else
        {
            std::cout << "Contact with this name not exist\n";
        }
    }
    else
    {
        return input;
    }
    return "";
}

bool Phone::nameCheck(const std::string& name)
{
    auto it = phonebook.find(name);
    if (it == phonebook.end())
    {
        return false;
    }
    return true;
}

bool Phone::numCheck(std::string num) const
{
    size_t found;
    
    if (num[0] != '+')
    {
        return false;
    }
    
    found = num.find_first_not_of("+0123456789()- ");
    if (found != std::string::npos)
    {
        return false;
    }
    
    found = num.find_first_not_of("0123456789");;    
    while (found != std::string::npos)
    {
        num.erase(found, 1);
        found = num.find_first_not_of("0123456789");
    }
    
    if (num.length() != 11)
    {
        return false;
    }

    return true;
}

void Phone::addNum(const std::string& num)
{
    std::string name;
    std::cout << "Enter contact name: ";
    std::cin >> name;
    if (nameCheck(name))
    {
        auto it = phonebook.find(name);
        it->second.addNum(num);
        std::cout << "Added\n";
    }
    else
    {
        std::string c;
        std::cout << "Contact with this name not exist. Create? y/n: ";
        std::cin >> c;
        if (c == "y")
        {
            create(name, num);
        }
        else
        {
            std::cout << "Operation failed\n";
        }
    }
}

void Phone::createEmpty(const std::string& name)
{
    if (!nameCheck(name))
    {
        Contact contact(name);
        phonebook.insert(std::make_pair(name, contact));
        std::cout << "Contact created\n"; 
    }
    else
    {
        std::cout << "Contact already exists\n";
    }  
}

void Phone::create(const std::string& name, const std::string& num)
{
    if (nameCheck(name))
    {
        auto it = phonebook.find(name);
        it->second.addNum(num);
    }
    else
    {
        Contact contact(name, num);
        phonebook.insert(std::make_pair(name, contact));
    }
    std::cout << "Contact created\n";    
}
