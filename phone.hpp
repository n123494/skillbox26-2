#pragma once
#include "contact.hpp"
#include <iostream>
#include <string>
#include <map>

class Phone
{
    std::map<std::string, Contact> phonebook;
    
    bool nameCheck(const std::string& name);
    
    bool numCheck(std::string num) const;
    
    void create(const std::string& name, const std::string& num);
    
    void createEmpty(const std::string& name);
    
    void addNum(const std::string& num);
    
    std::string getNum(const std::string& input) const;
    
public:
    Phone();
    ~Phone();
    
    void add(const std::string& input);
    
    void call(const std::string& input) const;
    
    void sms(const std::string& input) const;
};

